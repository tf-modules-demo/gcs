resource "google_storage_bucket" "bucket" {
  for_each      = var.name
  name          = each.key
  location      = "${var.location}"
  project       = each.value.project
  storage_class = each.value.storage_class
  force_destroy = "${var.force_destroy}"
}
